module gitlab.com/phuonghuynhdotnet/microservicesauthdemo

go 1.14

require (
	github.com/caarlos0/env/v6 v6.2.2
	github.com/casbin/casbin/v2 v2.2.2
	github.com/casbin/gorm-adapter/v2 v2.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/assert/v2 v2.0.1
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/protobuf v1.4.1
	github.com/jinzhu/gorm v1.9.12
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.5.1
	github.com/vektra/mockery v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	google.golang.org/grpc v1.28.1
	google.golang.org/protobuf v1.22.0
)
