package core

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type DBConfig struct {
	Driver string `env:"DB_DRIVER" envDefault:"postgres"`
	DSN    string `env:"DB_DSN" envDefault:"host=postgres port=5432 user=postgres dbname=post password=stupid"`
}

// NewDBConnection make a new connection
func NewDBConnection(config DBConfig) (*gorm.DB, error) {
	db, err := gorm.Open(config.Driver, config.DSN)
	if err != nil {
		return nil, err
	}

	return db, err
}

// CloseDB safety close DB
func CloseDB(db *gorm.DB) {
	if db != nil {
		_ = db.Close()
	}
}

// GetInMemoryConnection make an in-memory connection for testing
func GetInMemoryConnection() (*gorm.DB, error) {
	config := DBConfig{
		Driver: "sqlite3",
		DSN:    ":memory:",
	}
	return NewDBConnection(config)
}
