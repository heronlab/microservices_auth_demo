package core

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"reflect"
	"strings"
)

// NewValidator ...
func NewValidator() Validator {
	vr := validator.New()
	vr.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	return &validatorImpl{vr}
}

// Validator a go-playground validator wrapper for faster & easier
// get error on struct validation
type Validator interface {
	Struct(s interface{}) error
}

// ValidatorFieldError presents a field error (with name & message)
type ValidatorFieldError interface {
	GetField() string
	GetMessage() string
	Error() string
}

// ValidatorResult present a validation result
type ValidatorErrors interface {
	GetErrors() []ValidatorFieldError
	Error() string
}

type validatorImpl struct {
	validate *validator.Validate
}

type validatorFieldError struct {
	field   string
	message string
}

type validationErrors struct {
	fieldErrors []ValidatorFieldError
}

func (v *validationErrors) Error() string {
	return fmt.Sprintf("failed validation on %d field(s)", len(v.fieldErrors))
}

func (v *validationErrors) GetErrors() []ValidatorFieldError {
	return v.fieldErrors
}

func (v *validatorFieldError) GetField() string {
	return v.field
}

func (v *validatorFieldError) GetMessage() string {
	return v.message
}

func (v *validatorFieldError) Error() string {
	return fmt.Sprintf("%s: %s", v.GetField(), v.GetMessage())
}

func (v *validatorImpl) Struct(s interface{}) error {
	err := v.validate.Struct(s)
	if err == nil {
		return nil
	}

	if _, ok := err.(*validator.InvalidValidationError); ok {
		return err
	}

	var fields []ValidatorFieldError
	for _, e := range err.(validator.ValidationErrors) {
		field := &validatorFieldError{
			field: e.Field(),
		}
		tag := ""
		if e.Param() != "" {
			tag = fmt.Sprintf(" (param: %s, value: %v)", e.Param(), e.Value())
		}

		field.message = fmt.Sprintf("failed validation on tag %s%s", e.Tag(), tag)
		fields = append(fields, field)
	}

	return &validationErrors{fieldErrors: fields}
}
