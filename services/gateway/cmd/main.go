package main

import (
	"github.com/caarlos0/env/v6"
	"github.com/gin-gonic/gin"
	"gitlab.com/phuonghuynhdotnet/microservicesauthdemo/core"
	"gitlab.com/phuonghuynhdotnet/microservicesauthdemo/services/authx/pb"
	"gitlab.com/phuonghuynhdotnet/microservicesauthdemo/services/gateway"
	"gitlab.com/phuonghuynhdotnet/microservicesauthdemo/services/gateway/service"
	"google.golang.org/grpc"
	"net/http"

	"github.com/sirupsen/logrus"
)

var log *logrus.Entry

const (
	serviceName = "gateway"
	version     = "0.0.0"
)

func init() {
	log = core.MakeLoggerByEnv("service:" + serviceName)
}

// onboarding services on gateway, in real life we can do this dynamically
var gwServices = []service.OnboardingService{
	{
		ServiceID: "authx",
		Routes: []service.Route{
			{
				Path:              "/status",
				Method:            http.MethodGet,
				ServiceIDStripped: false,
				Authorizing:       false,
			},
			{
				Path:              "/login",
				Method:            http.MethodPost,
				ServiceIDStripped: true,
				Authorizing:       false,
			},
		},
	},
	{
		ServiceID: "blog",
		Routes: []service.Route{
			{
				Path:              "/status",
				Method:            http.MethodGet,
				ServiceIDStripped: false,
				Authorizing:       false,
			},
			{
				Path:              "/articles",
				Method:            http.MethodPost,
				ServiceIDStripped: true,
				Authorizing:       true,
			},
			{
				Path:              "/articles/*articleSuffix",
				Method:            http.MethodGet,
				ServiceIDStripped: true,
				Authorizing:       true,
			},
		},
	},
}

func main() {
	config := gateway.AppConfig{}
	_ = env.Parse(&config)

	// register routes
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(core.GlobalTracingHandler(serviceName), core.GlobalRequestLog(serviceName, log))

	r.GET("/status", core.HealthHandler(serviceName, version))

	// authx grpc connection
	var authxClient pb.AuthxClient
	if config.UseAuthxGRPC && len(config.AuthxAddress) > 0 {
		authxConn, err := grpc.Dial(config.AuthxAddress, grpc.WithInsecure())
		if err != nil {
			log.WithError(err).Error("failed to open connection to Authx(grpc)")
		} else {
			defer func () {
				_ = authxConn.Close()
			}()
			authxClient = pb.NewAuthxClient(authxConn)
		}
	}

	authPlugin := service.NewAuthPlugin(config.AuthServiceURL, authxClient)
	proxyPlugin := service.NewProxy(map[string]string{
		"authx": config.AuthServiceURL,
		"blog":  config.BlogServiceURL,
	}, log)

	onboardServices(r, authPlugin, proxyPlugin)

	v := make(chan struct{})
	go func() {
		if err := r.Run(config.ListenAddr); err != nil {
			log.WithError(err).Error("failed to start service")
			close(v)
		}
	}()
	log.WithField("listen_addr", config.ListenAddr).Info("start listening")
	<-v
}

func onboardServices(r *gin.Engine, authPlugin service.AuthPlugin, proxyPlugin service.ProxyPlugin) {
	for _, svc := range gwServices {
		for _, route := range svc.Routes {
			var handlers []gin.HandlerFunc
			if route.Authorizing {
				handlers = append(handlers, authPlugin.AuthorizeMiddleware())
			}

			routePath := route.Path
			// in case we don't need to strip service name
			// the route will be prefixed with service name
			// i.e service:authx, route:/status
			// will be publish on gateway as -> /authx/status
			// and proxy to authx as /status
			if !route.ServiceIDStripped {
				routePath = "/" + svc.ServiceID + route.Path
				handlers = append(handlers, proxyPlugin.Forward())
			} else {
				handlers = append(handlers, proxyPlugin.ForwardService(svc.ServiceID))
			}
			r.Handle(route.Method, routePath, handlers...)
		}
	}
}
