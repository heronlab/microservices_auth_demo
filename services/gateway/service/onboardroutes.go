package service

type Route struct {
	Path              string
	Method            string
	ServiceIDStripped bool
	Authorizing       bool
}

type OnboardingService struct {
	ServiceID string
	Routes    []Route
}
