package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"strings"
	"time"
)

type ProxyPlugin interface {
	Forward() gin.HandlerFunc
	ForwardService(s string) gin.HandlerFunc
}

type proxyImpl struct {
	servicesUrls map[string]string
	httpClient   *http.Client
	log          *logrus.Entry
}

func (p *proxyImpl) ForwardService(s string) gin.HandlerFunc {
	return func(c *gin.Context) {
		p.forwardToService(c, s)
	}
}

func (p *proxyImpl) Forward() gin.HandlerFunc {
	return func(c *gin.Context) {
		p.forwardToService(c, "")
	}
}

func (p *proxyImpl) forwardToService(c *gin.Context, s string) {
	targetPath := c.Request.URL.Path
	serviceName := s

	// try to guess service name
	if s == "" {
		fragments := strings.Split(strings.Trim(c.Request.URL.Path, "/"), "/")
		if len(fragments) < 1 || fragments[0] == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "unknown service"})
			return
		}
		serviceName = fragments[0]
		// remove service name from first fragment
		// /blog/articles/2 -> /article/2
		targetPath = "/" + strings.Join(fragments[1:], "/")
	}

	baseURL, found := p.servicesUrls[serviceName]
	if !found {
		c.JSON(http.StatusBadRequest, gin.H{"error": "service not found " + serviceName})
		c.Abort()
		return
	}

	if c.Request.URL.RawQuery != "" {
		targetPath += "?" + c.Request.URL.RawQuery
	}

	req, err := http.NewRequest(c.Request.Method, baseURL+targetPath, c.Request.Body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// forward authorized data
	if c.GetString("x-user-id") != "" {
		req.Header.Set("x-user-id", c.GetString("x-user-id"))
		req.Header.Set("x-user-role", c.GetString("x-user-role"))
	}
	req.Header.Add("x-trace", "gateway")
	req.Header.Add("user-agent", "gateway/1.0")

	start := time.Now()
	rsp, err := p.httpClient.Do(req)
	end := time.Now()
	latency := end.Sub(start)
	c.Header("content-type", "application/json")
	c.Header("x-upstream-latency", fmt.Sprintf("%d", latency.Milliseconds()))

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(rsp.StatusCode)
	c.Header("content-type", "application/json")
	c.Header("x-trace", rsp.Header.Get("x-trace"))
	_, _ = io.Copy(c.Writer, rsp.Body)
	_ = rsp.Body.Close()
}

func NewProxy(servicesUrls map[string]string, log *logrus.Entry) ProxyPlugin {
	// remove ending slash on host
	for k, v := range servicesUrls {
		servicesUrls[k] = strings.TrimRight(v, "/")
	}
	return &proxyImpl{
		servicesUrls: servicesUrls,
		httpClient:   &http.Client{Timeout: time.Second * 10},
		log:          log,
	}
}
