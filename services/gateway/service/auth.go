package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/phuonghuynhdotnet/microservicesauthdemo/services/authx/pb"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type AuthPlugin interface {
	AuthorizeMiddleware() gin.HandlerFunc
}

type authPluginImpl struct {
	authxServer string
	httpClient  *http.Client
	authxClient pb.AuthxClient
}

const (
	authPluginUserAgent = "gateway-auth/1.0"
)

func (a *authPluginImpl) AuthorizeMiddleware() gin.HandlerFunc {
	return a.authorize
}

func (a *authPluginImpl) authorize(c *gin.Context) {
	// extract token
	token := ""
	authHeader := c.GetHeader("authorization")
	if authHeader != "" {
		token = strings.Replace(authHeader, "Bearer ", "", 1)
	}

	if token == "" {
		token = c.Query("token")
	}
	if token == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "invalid authorization header"})
		c.Abort()
		return
	}

	reqData := &pb.ValidateRequest{
		Token: token,
		Scope: c.Request.URL.Path,
		Op:    c.Request.Method,
	}

	var rspData *pb.ValidateResponse
	if a.authxClient != nil {
		var err error
		rspData, err = a.authxClient.Validate(context.Background(), reqData)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
	} else {
		// request to authx
		url := a.authxServer + "/validate"
		bPayload, _ := json.Marshal(reqData)
		req, _ := http.NewRequest("POST", url, bytes.NewReader(bPayload))
		req.Header.Set("user-agent", authPluginUserAgent)
		rsp, err := a.httpClient.Do(req)
		if err != nil {
			msg := fmt.Sprintf("error while authorizing, #error: %s", err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": msg})
			c.Abort()
			return
		}
		if rsp.StatusCode != http.StatusOK {
			msg := fmt.Sprintf("error while authorizing, #status: %d", rsp.StatusCode)
			c.JSON(rsp.StatusCode, gin.H{"error": msg})
			c.Abort()
			return
		}
		rspData = &pb.ValidateResponse{}
		bPayload, err = ioutil.ReadAll(rsp.Body)
		_ = rsp.Body.Close()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "reading authorization response error"})
			return
		}
		err = json.Unmarshal(bPayload, &rspData)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "reading authorization response error"})
			return
		}

		if rspData.Code != http.StatusOK {
			c.JSON(int(rspData.Code), gin.H{"error": rspData.Error})
			return
		}
	}

	c.Set("x-user-id", fmt.Sprintf("%d", rspData.UserID))
	c.Set("x-user-role", rspData.Role)
	c.Next()
}

func NewAuthPlugin(authxServer string, authxClient pb.AuthxClient) AuthPlugin {
	plug := &authPluginImpl{
		authxServer: authxServer,
		httpClient:  &http.Client{Timeout: time.Second * 10},
		authxClient: authxClient,
	}
	return plug
}
