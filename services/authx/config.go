package authx

import "gitlab.com/phuonghuynhdotnet/microservicesauthdemo/core"

// AppConfig presents app config
type AppConfig struct {
	core.DBConfig

	SignKey        string `env:"SIGN_KEY" envDefault:"secret"`
	ListenAddr     string `env:"APP_LISTEN" envDefault:"0.0.0.0:8080"`
	GRPCListenAddr string `env:"GRPC_LISTEN" envDefault:"0.0.0.0:55051"`
}
