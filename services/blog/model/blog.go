package model

import "time"

type Article struct {
	ID        uint `gorm:"primary_key"`
	Title     string
	Body      string
	AuthorID  uint      `sql:"index"`
	CreatedAt time.Time `sql:"index"`
	UpdatedAt time.Time `sql:"index"`
}
