package blog

import "gitlab.com/phuonghuynhdotnet/microservicesauthdemo/core"

// AppConfig presents app config
type AppConfig struct {
	core.DBConfig

	ListenAddr string `env:"APP_LISTEN" envDefault:"0.0.0.0:8080"`
}
