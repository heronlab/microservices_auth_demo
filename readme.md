# Microservices Authentication & Authorization Demo


## Architecture

![architecture](https://s.lo4.co/200507.135318-qfmfm.png)


## Get started

Run stack

```
docker-compose build
docker-compose up
```

The gateway will be published at host port `8000`

Let give a try to health check endpoint:

```shell script
curl localhost:8000/status
```

```json
{"service":"gateway","status":"ok","version":"0.0.0"}
```

### Obtain token

Call login endpoint to take access token (with default credential as below)

- admin role: `{"username": "admin", "password": "admin"}`
- user role: `{"username": "user1", "password": "user1"}`

```shell script
curl -XPOST -d '{"username": "admin", "password": "admin"}' localhost:8000/login
```


you can have token like
```json
{"code":200,"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFkbWluIiwiZXhwIjoxNTg4OTIxMTYyLCJpYXQiOjE1ODg4MzQ3NjIsImlzcyI6ImF1dGh4In0.KoizAYDoJ-u9IILBt488pB_LU-G0rsG43f3-05_CIPs"}
```

### Create article
Let use that token to create article

```shell script
curl -XPOST -d '{"title": "test article", "body": "some great things are coming!"}' \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFkbWluIiwiZXhwIjoxNTg4OTIxMTYyLCJpYXQiOjE1ODg4MzQ3NjIsImlzcyI6ImF1dGh4In0.KoizAYDoJ-u9IILBt488pB_LU-G0rsG43f3-05_CIPs' \
    localhost:8000/articles
```

```json
{
  "id": 6,
  "title": "test article",
  "body": "some great things are coming!",
  "author_id": 1
}
```

### View an article

```shell script
curl localhost:8000/articles/1 \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFkbWluIiwiZXhwIjoxNTg4OTIxMTYyLCJpYXQiOjE1ODg4MzQ3NjIsImlzcyI6ImF1dGh4In0.KoizAYDoJ-u9IILBt488pB_LU-G0rsG43f3-05_CIPs'
```

```json
{
  "id": 1,
  "title": "test article",
  "body": "hello world",
  "author_id": 1
}
```
