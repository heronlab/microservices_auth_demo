#!/bin/bash

set -e

SERVER1='http://127.0.0.1:8000'
SERVER2='http://127.0.0.1:8001'

TOKEN=$(curl -s --location --request POST "${SERVER1}/login" \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFkbWluIiwiZXhwIjoxNTg4ODk2NjIzLCJpYXQiOjE1ODg4MTAyMjMsImlzcyI6ImF1dGh4In0.9NTr1IgODiSXTLyz7lVbCB8Rax5Cg4vPcmgCRjZgEWo' \
--header 'Content-Type: application/json' \
--data-raw '{
	"username": "admin",
	"password": "admin"
}' | jq -r '.token')


for server in "${SERVER1}" "${SERVER2}"; do
	echo "====== Server: ${server} ======"
	echo -e "GET ${server}/articles/1\nAuthorization: ${TOKEN}" | \
		vegeta attack -rate 200 -duration 20s | vegeta report
done
